<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<?php
include_once('Database.php');
include_once('controller.php');

if(isset($_GET["edit_id"])){
    $obj=new Database;
    foreach ($obj->queryById($_GET['edit_id']) as $value) ;
    extract($value);
}

if(isset($_POST['update'])){
    $con = new controller;
    $con->update($_POST["id"],$_POST["title"],$_POST["author"],$_POST["content"]);
}
/*
if(isset($_POST['update'])){
    $id = filter_var($_POST["id"], FILTER_SANITIZE_NUMBER_INT);
    $title = filter_var($_POST["title"], FILTER_SANITIZE_STRING);
    $author = filter_var($_POST["author"], FILTER_SANITIZE_STRING);
    $content = filter_var($_POST["content"], FILTER_SANITIZE_STRING);
    if ($obj->UpdateData($id,$title, $author, $content)) {
        header("location:show.php?status=success");
    }
}*/

?>
<body>
<div class="container-fluid">
    <table width="100%" height="320" border="0" cellpadding="8" cellspacing="1" bgcolor="#000000">
        <tr>
            <td height="89" colspan="2" class="jumbotron"><strong><h2>Blog System</h2></strong></td>
        </tr>
    <tr>
        <td width="80" align="left" valign="top" bgcolor="#FFFFFF">
            <h4><a>&nbsp</a></h4>
            <h4><a href="show.php">Back</a></h4>
        <td width="837" valign="top" bgcolor="#FFFFFF"><table width="743" border="0" cellpadding="8" cellspacing="1" bgcolor="#000000">
                <form id="blog1" name="blog1" method="post" action="edit.php">
                    <div class="form-horizontal">
                    <table width="779" border="0" cellpadding="8" cellspacing="1">
                        <tr>
                            <td colspan="5" align="left" bgcolor="#FFFFFF"><h4>&nbsp</h4></td>
                        </tr>
                    <tr>
                        <th>ID</th>
                        <td><input type="text" style="background-color: #8A8D8A" name="id" id="id" readonly="readonly" value="<?php echo $value['id']?>"/></td>
                    </tr>
                        <tr>
                            <td colspan="5" align="left" bgcolor="#FFFFFF"><a>&nbsp</a></td>
                        </tr>
                    <tr>
                        <th>Title</th>
                        <td><input type="text" name="title" id="Title" value="<?php echo $value['Title']?>"/></td>
                    </tr>
                        <tr>
                            <td colspan="5" align="left" bgcolor="#FFFFFF"><a>&nbsp</a></td>
                        </tr>
                    <tr>
                        <th>Author</th>
                        <td><input type="text" name="author" id="Author" value="<?php echo $value['Author']?>"/></td>
                    </tr>
                        <tr>
                            <td colspan="5" align="left" bgcolor="#FFFFFF"><a>&nbsp</a></td>
                        </tr>
                    <tr>
                        <th>Content</th>
                        <td><textarea name="content" cols="60" rows="5" id="Content"><?php echo $value['Content']?></textarea></td>
                    </tr>
                    <tr>
                        <td align="left"><button type="Submit" name="update" id="update" class="btn"/>Update</button></td>
                    </tr>
                    </table>
                    </div>
                </form>
                </td>
    </tr>
</table>
</div>
</body>
</html>