<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<?php
include_once('Database.php');
include_once('controller.php');

if(isset($_GET['del_id'])){
    $con = new controller;
    $con->del($_GET['del_id']);
}
/*
if(isset($_GET['del_id'])){
    $obj=new Database;
    if($obj->deleteData($_GET['del_id'])) {
    echo "  Your data has been deleted";
    }
}*/
?>

<body>
<div class="container-fluid">
<table width="100%" height="320" border="0" cellpadding="8" cellspacing="1" bgcolor="#000000">
    <tr>
        <td height="89" colspan="2" class="jumbotron"><strong><h2>Blog System</h2></strong></td>
    </tr>
    <td width="80" align="left" valign="top" bgcolor="#FFFFFF">
        <h4><a>&nbsp</a></h4>
        <h4><a href="show.php">Home</a></h4>
        <h4><a href="add.php">Add</a></h4></td>
    <td width="637" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="12" cellspacing="10" bgcolor="#000000">
            <tr>
                <td colspan="5" align="left" bgcolor="#FFFFFF"><h4>&nbsp</h4></td>
            </tr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="span6">
                        </div>
                        <div class="span4">
                            <table class="table">
                                <thead>
                                <tr class="success">
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Content</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $obj=new Database;
                                foreach($obj->querytable("blog") as $value){
                                    extract($value);
                                    ?>
                                    <tr class="info">
                                        <td bgcolor="#FFFFFF"><?php echo $value['id']?></td>
                                        <td bgcolor="#FFFFFF"><?php echo $value['Title']?></td>
                                        <td bgcolor="#FFFFFF"><?php echo $value['Author']?></td>
                                        <td bgcolor="#FFFFFF"><?php echo $value['Content']?></td>
                                        <td bgcolor="#FFFFFF"><a href="show.php?del_id=<?php echo $value['id']; ?>">Delete</a>&nbsp<a href="edit.php?edit_id=<?php echo $value['id']; ?>">Edit</a></td>
                                    </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </table></td>
    </tr>
</table>
</div>
</body>
</html>




