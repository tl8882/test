<?php

class Database
{
    private $host = "127.0.0.1";
    private $user = "root";
    private $pass = "";
    private $dbname = "test";
    private $conn;

    public function __construct()
    {
        $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->dbname, $this->user, $this->pass);
    }//Connect to database

    public function querytable()
    {
        try {
            $sql = "SELECT * FROM blog";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $rows;
        } catch (PDOException $pe) {
            //write to log
            var_dump($pe);
            exit;
        }
    }//query whole table

    public function queryById($id)
    {
        try {
            $sql = "SELECT * FROM blog WHERE id=:id";
            $stmt = $this->conn->prepare($sql);
            $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute(array(":id" => $id));
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $rows;
        } catch (PDOException $pe) {
            //write to log
            var_dump($pe);
            exit;
        }
    }//query by id

    public function insertData($title, $author, $content)
    {
        try {
            $sql = "INSERT INTO blog SET Title=:title, Author=:author, Content=:content";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(":title", $title);
            $stmt->bindParam(":author", $author);
            $stmt->bindParam(":content", $content);
            $stmt->execute(array(":title" => $title, ":author" => $author, ":content" => $content));
            return true;
        } catch (PDOException $pe) {
            //write to log
            var_dump($pe);
            exit;
        }
    }//insert new

    public function UpdateData($id, $title, $author, $content)
    {
        try {
            $sql = "UPDATE blog SET Title=:title, Author=:author, Content=:content WHERE id=:id";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(":title", $title);
            $stmt->bindParam(":author", $author);
            $stmt->bindParam(":content", $content);
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute(array(":id" => $id, ":title" => $title, ":author" => $author, ":content" => $content));
            return true;
        } catch (PDOException $pe) {
            //write to log
            var_dump($pe);
            exit;
        }
    }//update data

    public function deleteData($id)
    {
        try {
            $sql = "DELETE FROM blog WHERE id=:id";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute(array(":id" => $id));
            return true;
        } catch (PDOException $pe) {
            //write to log
            var_dump($pe);
            exit;
        }
    }//delete by id
}
?>