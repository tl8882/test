<?php

include_once('Database.php');

class controller
{
    public function del($id){
        $obj=new Database;
        if($obj->deleteData($id)) {
            echo "Your data has been deleted";
        }else{
            echo "Oops";
        }
    }//delete controller

    public function submit($title,$author,$content){
        $new = new controller;
        if(!($new->isEmpty($title))){
            $title = $new->filterString($title);
            //$title = filter_var($title, FILTER_SANITIZE_STRING);
        }
        $obj = new Database;
        $author = $new->filterString($author);
        $content = $new->filterString($content);
        //$author = filter_var($author, FILTER_SANITIZE_STRING);
        //$content = filter_var($content, FILTER_SANITIZE_STRING);
        if($obj->insertData($title,$author,$content)){
            header("location:show.php?status_insert=success");
        }
    }//submit controller

    public function update($id,$title,$author,$content){
        $obj = new Database;
        $new = new controller;
        $id = $new->filterString($id);
        $title = $new->filterString($title);
        $author = $new->filterString($author);
        $content = $new->filterString($content);
        /*$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $title = filter_var($title, FILTER_SANITIZE_STRING);
        $author = filter_var($author, FILTER_SANITIZE_STRING);
        $content = filter_var($content, FILTER_SANITIZE_STRING);*/
        if ($obj->UpdateData($id, $title, $author, $content)) {
            header("location:show.php?status=success");
        }
    }//update controller

    public function isEmpty($input){
        return empty($input);
    }

    public function filterString($input){
        $input = filter_var($input, FILTER_SANITIZE_STRING);
        return $input;
    }

}